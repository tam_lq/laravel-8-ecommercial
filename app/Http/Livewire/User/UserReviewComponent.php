<?php

namespace App\Http\Livewire\User;

use App\Models\OrderItem;
use App\Models\Review;
use Livewire\Component;

class UserReviewComponent extends Component
{
    public $order_item_id;
    public $rating;
    public $comment;

    protected $rules = [
        'rating' => 'required',
        'comment' => 'required'
    ];

    public function mount($order_item_id)
    {
        $this->order_item_id = $order_item_id;
    }

    public function updated($field)
    {
        $this->validateOnly($field, $this->rules);
    }

    public function addReview()
    {
        $this->validate();
        $review = new Review();
        $review->rating = $this->rating;
        $review->comment = $this->comment;
        $review->order_item_id = $this->order_item_id;
        $review->save();

        $orderItem = OrderItem::find($this->order_item_id);
        $orderItem->rvstatus = 1;
        $orderItem->save();

        session()->flash('review_message', 'Your review has been posted successfully!');
    }
    public function render()
    {
        $orderItem = OrderItem::with('order')->find($this->order_item_id);
        return view('livewire.user.user-review-component', compact('orderItem'))->layout('layouts.base');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use App\Models\Category;
use App\Models\HomeCategory;
use Livewire\Component;

class AdminHomeCategoryComponent extends Component
{
    public $selected_categories = [];
    public $no_of_products;

    protected $rules = [
        'selected_categories' => 'required',
        'no_of_products' => 'required'
    ];
    public function mount()
    {
        $category = HomeCategory::find(1);
        $this->selected_categories = explode(',', $category->sel_categories);
        $this->no_of_products = $category->no_of_products;
    }

    public function update()
    {
        $this->validate();
        $category = HomeCategory::find(1);
        $category->sel_categories = implode(',', $this->selected_categories);
        $category->no_of_products = $this->no_of_products;
        $category->save();
        session()->flash('message', 'Home Category has been updated successfully!');
    }
    public function render()
    {
        $categories = Category::all();
        return view('livewire.admin.admin-home-category-component', compact('categories'))->layout('layouts.base');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use App\Models\Coupon;
use Livewire\Component;

class AdminCreateCouponComponent extends Component
{
    public $code;
    public $type;
    public $value;
    public $cart_value;
    public $expiry_date;

    protected $rules = [
        'code' => 'required|unique:coupons,code',
        'type' => 'required',
        'value' => 'required|numeric',
        'cart_value' => 'required|numeric',
        'expiry_date' => 'required'
    ];
    public function mount()
    {
        $this->type = 'fixed';
    }

    public function updated($field)
    {
        $this->validateOnly($field, $this->rules);
    }

    public function store()
    {
        $this->validate();
        $coupon = new Coupon();
        $coupon->code = $this->code;
        $coupon->type = $this->type;
        $coupon->value = $this->value;
        $coupon->cart_value = $this->cart_value;
        $coupon->expiry_date = $this->expiry_date;
        $coupon->save();
        session()->flash('message', 'Coupon has been created successfully!');
    }

    public function render()
    {
        return view('livewire.admin.admin-create-coupon-component')->layout('layouts.base');
    }
}

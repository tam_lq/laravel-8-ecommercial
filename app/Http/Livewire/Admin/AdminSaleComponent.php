<?php

namespace App\Http\Livewire\Admin;

use App\Models\Sale;
use Livewire\Component;

class AdminSaleComponent extends Component
{
    public $sale_date;
    public $status;

    protected $rules = [
        'sale_date' => 'required',
        'status' => 'required'
    ];
    public function mount()
    {
        $sale = Sale::find(1);
        $this->sale_date = $sale->sale_date;
        $this->status = $sale->status;
    }

    public function updated($field)
    {
        $this->validateOnly($field, $this->rules);
    }

    public function updateSale()
    {
        $this->validate();
        $sale = Sale::find(1);
        $sale->status = $this->status;
        $sale->sale_date = $this->sale_date;
        $sale->save();
        session()->flash('message', 'Sale has been updated successfully!');
    }

    public function render()
    {
        return view('livewire.admin.admin-sale-component')->layout('layouts.base');
    }
}

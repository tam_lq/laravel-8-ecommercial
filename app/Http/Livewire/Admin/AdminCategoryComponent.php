<?php

namespace App\Http\Livewire\Admin;

use App\Models\Category;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;

class AdminCategoryComponent extends Component
{
    use WithPagination;

    public $category_id = null;
    public $name;
    public $slug;

    public function confirmCategoryRemoval($id = null)
    {
        if ($id) {
            $this->category_id = $id;
        } else $this->category_id = null;
    }

    protected $rules = [
        'name' => 'required',
        'slug' => 'required|unique:categories,slug'
    ];

    public function updated($field)
    {
        $this->validateOnly($field, $this->rules);
    }

    public function generateSlug()
    {
        $this->slug = Str::slug($this->name);
    }

    public function store()
    {
        $this->validate();
        $category = new Category();
        $category->name = $this->name;
        $category->slug = $this->slug;
        $category->save();
        session()->flash('message', 'Category has been created successfully!');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        session()->flash('message', 'Category has been deleted successfully!');
    }

    public function render()
    {
        $categories = Category::paginate(5);
        return view('livewire.admin.admin-category-component', compact('categories'))->layout('layouts.base');
    }
}

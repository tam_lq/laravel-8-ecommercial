<?php

namespace App\Http\Livewire\Admin;

use App\Models\HomeSlider;
use Livewire\Component;

class AdminHomeSliderComponent extends Component
{
    public $slider_id;

    public function confirmDestroySlide($id = null)
    {
        $this->slider_id = $id;
    }

    public function destroy($id)
    {
        HomeSlider::destroy($id);
        session()->flash('message', 'Slide has been deleted successfully!');
    }
    public function render()
    {
        $sliders = HomeSlider::all();
        return view('livewire.admin.admin-home-slider-component', compact('sliders'))->layout('layouts.base');
    }
}

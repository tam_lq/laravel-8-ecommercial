<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;
use Cart;

class ShopComponent extends Component
{
    use WithPagination;

    public $sorting;
    public $pagesize;

    // Filter price
    public $min_price;
    public $max_price;

    public function mount()
    {
        $this->sorting = "default";
        $this->pagesize = 12;

        $this->min_price = 1;
        $this->max_price = 1000;
    }

    public function store($product_id, $product_name, $product_price)
    {
        Cart::instance('cart')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        session()->flash('success_message', 'Item added in Cart');
        return redirect()->route('product.cart');
    }

    public function addToWishlist($product_id, $product_name, $product_price)
    {
        Cart::instance('wishlist')->add($product_id, $product_name, 1, $product_price)->associate('App\Models\Product');
        $this->emitTo('wishlist-count-component', 'refreshComponent');
        session()->flash('success_message', 'Item added in Wishlist');
    }

    public function removeFromWishlist($product_id)
    {
        foreach (Cart::instance('wishlist')->content() as $item) {
            if ($item->id === $product_id) {
                Cart::instance('wishlist')->remove($item->rowId);
                $this->emitTo('wishlist-count-component', 'refreshComponent');
                return;
            }
        }
    }

    public function render()
    {
        $priceBetween = [$this->min_price, $this->max_price];
        switch ($this->sorting) {
            case 'date':
                $products = Product::whereBetween('regular_price', $priceBetween)->orderBy('created_at', 'desc')->paginate($this->pagesize);
                break;
            case 'price':
                $products = Product::whereBetween('regular_price', $priceBetween)->orderBy('regular_price', 'asc')->paginate($this->pagesize);
                break;

            case 'price-desc':
                $products = Product::whereBetween('regular_price', $priceBetween)->orderBy('regular_price', 'desc')->paginate($this->pagesize);
                break;

            default:
                $products = Product::whereBetween('regular_price', $priceBetween)->paginate($this->pagesize);
                break;
        }

        $categories = Category::all();
        return view('livewire.shop-component', ['products' => $products, 'categories' => $categories])->layout('layouts.base');
    }
}

<?php

namespace App\Listeners;

use App\Events\OrderSucceededEvent;
use App\Mail\OrderSucceededMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendOrderSucceededNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param OrderSucceededEvent $event
     * @return void
     */
    public function handle(OrderSucceededEvent $event)
    {
        $dataInvoice = [
            'order_id' => $event->order->id,
            'total' => $event->order->total,
            'full_name' => $event->order->full_name,
            'email' => $event->order->email,
            'items' => $event->order->orderItems,
            'phone' => $event->order->mobile,
            'line1' => $event->order->line1,
            'province' => $event->order->province,
            'country' => $event->order->country,
            'city' => $event->order->city,
            'tax' => $event->order->tax,
            'discount' => $event->order->discount,
        ];
        Mail::to($event->order->email)->send(new OrderSucceededMail($dataInvoice));
    }
}

<div>
    <div class="container" style="padding: 30px 0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Add New Product
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.products.index') }}" class="btn btn-success pull-right">All
                                    Products</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <form action="" class="form-horizontal" enctype='multipart/form-data'
                              wire:submit.prevent='store'>

                            <x-input.group label="Product Name" error="name" wireIgnore="true">
                                <x-input.text id='name' placeholder='Product Name'
                                              wire:model='name' wire:keyup='generateSlug'/>
                            </x-input.group>

                            <x-input.group label="Slug" error="slug" wireIgnore="true">
                                <x-input.text id='slug' placeholder='Slug'
                                                  wire:model='slug'/>
                            </x-input.group>

                            <x-input.group label="Short Description" error="short_description" wireIgnore="true">
                                <x-input.textarea id='short_description' placeholder='Short Description'
                                                  wire:model='short_description'/>
                            </x-input.group>

                            <x-input.group label="Description" error="description" wireIgnore="true">
                                <x-input.textarea id='description' placeholder='Description' wire:model='description'/>
                            </x-input.group>

                            <x-input.group label="Regular Price" error="regular_price" required="true">
                                <x-input.text id='regular_price' placeholder='Regular Price'
                                              wire:model='regular_price'/>
                            </x-input.group>

                            <x-input.group label="Sale Price" error="sale_price">
                                <x-input.text id='sale_price' placeholder='Sale Price' wire:model='sale_price'/>
                            </x-input.group>

                            <x-input.group label="SKU" error="SKU" required="true">
                                <x-input.text id='SKU' placeholder='SKU' wire:model='SKU'/>
                            </x-input.group>

                            @php
                                $stocks = [['key' => 'instock','value' => 'In Stock'],['key' => 'outofstock','value' => 'Out Of Stock']];
                                $features = [['key' => 0,'value' => 'No'], ['key' =>1, 'value' =>  'Yes']];
                            @endphp

                            <x-input.group label="Stock" error="stock_status">
                                <x-select id='stock_status' :options="$stocks" placeholder='Stock'
                                          wire:model='stock_status'/>
                            </x-input.group>

                            <x-input.group label="Featured" error="featured">
                                <x-select id='featured'
                                          :options="$features"
                                          placeholder='Featured' wire:model='featured'/>
                            </x-input.group>

                            <x-input.group label="Quantity" error="quantity" required="true">
                                <x-input.text id='quantity' placeholder='Quantity' wire:model='quantity'/>
                            </x-input.group>

                            <x-input.group label="Single Image" error="image" required="true">
                                <input type="file" class='input-md' wire:model='image'/>
                                @if ($image)
                                    <img src="{{ $image->temporaryUrl() }}" alt="{{ $name }}"
                                         width='120'/>
                                @endif
                            </x-input.group>

                            <x-input.group label="Gallery Image" error="images" required="true">
                                <input type="file" class='input-md' multiple wire:model='images'/>
                                @if ($images)
                                    @foreach ($images as $image)
                                        <img src="{{ $image->temporaryUrl() }}" alt="{{ $name }}"
                                             width='120'/>
                                    @endforeach
                                @endif
                            </x-input.group>

                            <x-input.group label="Category" error="category_id">
                                <x-select id='category_id'
                                          :options="$categories"
                                          placeholder='Category' wire:model='category_id'>
                                    <option value="">Select Category</option>
                                </x-select>
                            </x-input.group>

                            <div class="form-group ">
                                <label for="" class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class='btn btn-primary'>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function () {
            let options = {}
            // if(window.location)
            if (window.location.origin === 'http://localhost' || window.location.origin ===
                'http://ecommercial.test') {
                options = {
                    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
                    toolbar_mode: 'floating',
                    tinycomments_mode: 'embedded',
                    tinycomments_author: 'Author name',
                }
            }
            tinymce.init({
                selector: '#short_description',
                setup: function (editor) {
                    editor.on('Change', function (e) {
                        tinyMCE.triggerSave();
                        let s_description = $('#short_description').val();
                    @this.set('short_description', s_description);
                    })
                },
                ...options
            });

            tinymce.init({
                selector: '#description',
                setup: function (editor) {
                    editor.on('Change', function (e) {
                        tinyMCE.triggerSave();
                        let description = $('#description').val();
                    @this.set('description', description);
                    })
                },
                ...options
            })
        })
    </script>
@endpush

<div>
    <div class="container" style="padding: 30px 0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Edit Product
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.products.index') }}" class="btn btn-success pull-right">All
                                    Products</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <form action="" class="form-horizontal" enctype='multipart/form-data'
                            wire:submit.prevent='update'>
                            <div class="form-group @error('name') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Product Name <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" placeholder='Product Name' wire:keyup='generateSlug'
                                        class='form-control input-md' wire:model='name' />
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('slug') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Product Slug <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" disabled placeholder='Product Slug' class='form-control input-md'
                                        wire:model='slug' />
                                    @error('slug')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('short_description') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Short Description</label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea type="text" id='short_description' placeholder='Short Description'
                                        class='form-control input-md' wire:model='short_description'></textarea>
                                    @error('short_description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('description') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Description <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea type="text" id='description' placeholder='Description'
                                        class='form-control input-md' wire:model='description'></textarea>
                                    @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('regular_price') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Regular Price <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" placeholder='Regular Price' class='form-control input-md'
                                        wire:model='regular_price' />
                                    @error('regular_price')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('sale_price') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Sale Price</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder='Sale Price' class='form-control input-md'
                                        wire:model='sale_price' />
                                    @error('sale_price')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('SKU') has-error @enderror">
                                <label for="" class="col-md-4 control-label">SKU <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" placeholder='SKU' class='form-control input-md'
                                        wire:model='SKU' />
                                    @error('SKU')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('stock_status') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Stock</label>
                                <div class="col-md-4">
                                    <select wire:model='stock_status' class="form-control">
                                        <option value="instock">In Stock</option>
                                        <option value="outofstock">Out of Stocks</option>
                                    </select>
                                    @error('stock_status')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('featured') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Featured</label>
                                <div class="col-md-4">
                                    <select wire:model='featured' class="form-control">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                    @error('featured')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('quantity') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Quantity</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder='Quantity' class='form-control input-md'
                                        wire:model='quantity' />
                                    @error('quantity')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('image') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Product Image</label>
                                <div class="col-md-4">
                                    <input type="file" class='input-md' wire:model='newImage' />
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    @if ($newImage)
                                        <img src="{{ $newImage->temporaryUrl() }}" alt="{{ $name }}"
                                            width='120' />
                                    @else
                                        <img src="{{ asset('assets/images/products') }}/{{ $image }}"
                                            alt="{{ $name }}" width='120' />
                                    @endif
                                </div>
                            </div>

                            <div class="form-group @error('newImages') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Product Gallery</label>
                                <div class="col-md-4">
                                    <input type="file" class='input-md' multiple wire:model='newImages' />
                                    @error('newImages')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    @if ($newImages)
                                        @foreach ($newImages as $newImage)
                                            @if ($newImage)
                                                <img src="{{ $newImage->temporaryUrl() }}" alt="{{ $name }}"
                                                    width='120' />
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach ($images as $image)
                                            @if ($image)
                                                <img src="{{ asset('assets/images/products') }}/{{ $image }}"
                                                    alt="{{ $name }}" width='120' />
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="form-group @error('category_id') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Category</label>
                                <div class="col-md-4">
                                    <select wire:model='category_id' class="form-control">
                                        <option value="">Select Category</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>

                                        @endforeach
                                    </select>
                                    @error('category_id')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="" class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class='btn btn-primary'>Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {
            tinymce.init({
                selector: '#short_description',
                setup: function(editor) {
                    editor.on('Change', function(e) {
                        tinyMCE.triggerSave();
                        let s_description = $('#short_description').val();
                        @this.set('short_description', s_description);
                    })
                }
                //         plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                //   toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
                //   toolbar_mode: 'floating',
                //   tinycomments_mode: 'embedded',
                //   tinycomments_author: 'Author name',
            });

            tinymce.init({
                selector: '#description',
                setup: function(editor) {
                    editor.on('Change', function(e) {
                        tinyMCE.triggerSave();
                        let description = $('#description').val();
                        @this.set('description', description);
                    })
                }
            })
        })
    </script>
@endpush

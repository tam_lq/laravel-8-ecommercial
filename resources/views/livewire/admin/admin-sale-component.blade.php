<div>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Sale Setting</div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <form action="" class="form-horizontal" wire:submit.prevent='updateSale'>
                            <div class="form-group @error('status') has-error @enderror">
                                <label for="" class="col-md-4 coltrol-label">Status</label>
                                <div class="col-md-4">
                                    <select class='form-control' wire:model='status'>
                                        <option value="0">Inactive</option>
                                        <option value="1">Active</option>
                                    </select>
                                    @error('status')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('sale_date') has-error @enderror">
                                <label for="" class="col-md-4 coltrol-label">Sale Date</label>
                                <div class="col-md-4">
                                    <input type="text" id='sale-date' placeholder="YYYY/MM/DD H:M:S"
                                        wire:model='sale_date' class='form-control input-md' />
                                    @error('sale_date')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 coltrol-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {
            $('#sale-date').datetimepicker({
                format: 'Y-MM-DD h:m:s',
            }).on('dp.change', function(e) {
                let data = $('#sale-date').val();
                @this.set('sale_date', data);
            })
        })
    </script>
@endpush

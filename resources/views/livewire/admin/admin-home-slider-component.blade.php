<div>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">All Sliders</div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.home.slider.create') }}"
                                    class='btn btn-success pull-right'>Add New</a>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Subtitle</th>
                                    <th>Price</th>
                                    <th>Link</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sliders as $slider)
                                    <tr>
                                        <td>{{ $slider->id }}</td>
                                        <td>
                                            <img src="{{ asset('assets/images/sliders') }}/{{ $slider->image }}"
                                                alt="{{ $slider->title }}" width='120' />
                                        </td>
                                        <td>{{ $slider->title }}</td>
                                        <td>{{ $slider->subtitle }}</td>
                                        <td>{{ $slider->price }}</td>
                                        <td>{{ $slider->link }}</td>
                                        <td>{{ $slider->status === 1 ? 'Active' : 'Inactive' }}</td>
                                        <td>{{ $slider->created_at }}</td>
                                        <td>
                                            <a
                                                href="{{ route('admin.home.slider.edit', ['slider_id' => $slider->id]) }}"><i
                                                    class="fa fa-edit fa-2x text-info"></i></a>
                                            @if ($slider_id === $slider->id)
                                                <a href="#" wire:click.prevent="destroy({{ $slider->id }})"
                                                    style='margin-left: 10px;' class='text-success'><i
                                                        class="fa fa-check fa-2x"></i>Sure?</a>
                                                <a href="#" wire:click.prevent="confirmDestroySlide()"
                                                    style='margin-left: 10px;' class='text-danger'><i
                                                        class="fa fa-times fa-2x"></i>Cancel</a>
                                            @else
                                                <a href="#"
                                                    wire:click.prevent="confirmDestroySlide({{ $slider->id }})"
                                                    style='margin-left: 10px;'><i
                                                        class="fa fa-times fa-2x text-danger"></i></a>
                                            @endif
                                        </td>
                                    </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

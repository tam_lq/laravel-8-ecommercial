<div>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">Edit Slide</div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.home.slider.index') }}"
                                    class='btn btn-success pull-right'>All
                                    Slides</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                        <form action="" class="form-horizontal" wire:submit.prevent='update'>
                            <div class="form-group @error('title') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Title <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4"><input type="text" placeholder='Title'
                                        class='form-control input-md' wire:model='title' />
                                    @error('title')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('subtitle') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Subtitle <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4"><input type="text" placeholder='Subtitle'
                                        class='form-control input-md' wire:model='subtitle' />
                                    @error('subtitle')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('price') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Price <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4"><input type="text" placeholder='Price'
                                        class='form-control input-md' wire:model='price' />
                                    @error('price')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('link') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Link <span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4"><input type="text" placeholder='Link'
                                        class='form-control input-md' wire:model='link' />
                                    @error('link')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('newImage') has-error @enderror">
                                <label for="" class="col-md-4 control-label">Image<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4"><input type="file" placeholder='Image' class='input-md'
                                        wire:model='newImage' />
                                    @if ($newImage)
                                        <img src="{{ $newImage->temporaryUrl() }}" alt="{{ $title }}"
                                            width='120'>
                                    @else
                                        <img src="{{ asset('assets/images/sliders') }}/{{ $image }}"
                                            alt="{{ $title }}" width='120'>
                                    @endif
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label">Status </label>
                                <div class="col-md-4">
                                    <select wire:model='status' class='form-control input-md'>
                                        <option value="0">Inactive</option>
                                        <option value="1">Active</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type='submit' class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

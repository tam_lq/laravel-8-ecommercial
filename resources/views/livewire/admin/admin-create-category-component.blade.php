<div>
    <div class="container" style='padding: 30px 0'>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">Add New Category</div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.categories.index') }}" class='btn btn-success pull-right'>All
                                    Category</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session()->has('message'))
                            <div class="alert alert-success" role="alert">{{ session()->get('message') }}</div>
                        @endif
                            <form class="form-horizontal" wire:submit.prevent="store">
                                <x-input.group label="Category Name" error="name">
                                    <x-input.text wire:model="name" placeholder="Category Name" wire:keyup="generateSlug"/>
                                </x-input.group>

                                <x-input.group label="Category Slug" error="slug">
                                    <x-input.text wire:model="slug" placeholder="Category Slug" />
                                </x-input.group>

                                <div class="form-group">
                                    <label for="" class="col-md-4 control-label"></label>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

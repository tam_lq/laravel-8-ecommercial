<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contacts')->delete();
        
        \DB::table('contacts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'NoName',
                'email' => 'noemail@gmail.com',
                'phone' => '0123456789',
                'comment' => 'This is my comment
',
                'created_at' => '2021-10-05 20:55:58',
                'updated_at' => '2021-10-05 20:55:58',
            ),
        ));
        
        
    }
}
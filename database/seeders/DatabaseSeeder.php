<?php

namespace Database\Seeders;

use App\Models\Membership;
use App\Models\Sale;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(CouponsTableSeeder::class);
        $this->call(HomeCategoriesTableSeeder::class);
        $this->call(HomeSlidersTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(OrderItemsTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);
        $this->call(SalesTableSeeder::class);
        $this->call(SessionsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(ShippingsTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
    }
}

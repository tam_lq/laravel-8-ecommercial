<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SalesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sales')->delete();
        
        \DB::table('sales')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sale_date' => '2021-10-29 12:00:00',
                'status' => 1,
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-10-05 20:49:28',
            ),
        ));
        
        
    }
}
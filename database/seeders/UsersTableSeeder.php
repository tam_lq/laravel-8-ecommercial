<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$pGGqKcjY0tciFR8MXb/W2OfzW28NHZdtnz/A1.2AUzW3xfI8..KBi',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => '/no-avatar.png',
                'utype' => 'ADM',
                'created_at' => '2021-08-12 15:17:54',
                'updated_at' => '2021-08-12 15:17:54',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Akeem Taylor',
                'email' => 'user@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$KmEKI0/sVqP8ennOT3o4WO./6rA6DYNmk5UyipInkXEAcwbjSjlMC',
                'remember_token' => NULL,
                'current_team_id' => NULL,
                'profile_photo_path' => '/no-avatar.png',
                'utype' => 'USR',
                'created_at' => '2021-10-05 20:41:39',
                'updated_at' => '2021-10-05 20:41:39',
            ),
        ));
        
        
    }
}
<?php

use App\Http\Livewire\User\UserChangePasswordComponent;
use App\Http\Livewire\User\UserDashboardComponent;
use App\Http\Livewire\User\UserOrderComponent;
use App\Http\Livewire\User\UserOrderDetailsComponent;
use App\Http\Livewire\User\UserReviewComponent;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum', 'verified'])->prefix('user')->group(function () {
    Route::get('/dashboard', UserDashboardComponent::class)->name('user.dashboard');
    Route::get('/orders', UserOrderComponent::class)->name('user.orders.index');
    Route::get('/orders/{order_id}', UserOrderDetailsComponent::class)->name('user.orders.show');
    Route::get('/review/{order_item_id}', UserReviewComponent::class)->name('user.order.review');
    Route::get('/user/change-password', UserChangePasswordComponent::class)->name('user.password.change');
});

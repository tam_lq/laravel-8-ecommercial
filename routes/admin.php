<?php

use App\Http\Livewire\Admin\AdminCategoryComponent;
use App\Http\Livewire\Admin\AdminContactComponent;
use App\Http\Livewire\Admin\AdminCouponComponent;
use App\Http\Livewire\Admin\AdminCreateCategoryComponent;
use App\Http\Livewire\Admin\AdminCreateCouponComponent;
use App\Http\Livewire\Admin\AdminCreateHomeSliderComponent;
use App\Http\Livewire\Admin\AdminCreateProductComponent;
use App\Http\Livewire\Admin\AdminDashboardComponent;
use App\Http\Livewire\Admin\AdminEditCategoryComponent;
use App\Http\Livewire\Admin\AdminEditCouponComponent;
use App\Http\Livewire\Admin\AdminEditHomeSliderComponent;
use App\Http\Livewire\Admin\AdminEditProductComponent;
use App\Http\Livewire\Admin\AdminHomeCategoryComponent;
use App\Http\Livewire\Admin\AdminHomeSliderComponent;
use App\Http\Livewire\Admin\AdminOrderComponent;
use App\Http\Livewire\Admin\AdminOrderDetailsComponent;
use App\Http\Livewire\Admin\AdminProductComponent;
use App\Http\Livewire\Admin\AdminSaleComponent;
use App\Http\Livewire\Admin\AdminSettingComponent;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum', 'verified', 'auth.admin'])->prefix('admin')->group(function () {
    Route::get('/dashboard', AdminDashboardComponent::class)->name('admin.dashboard');

    Route::get('/categories', AdminCategoryComponent::class)->name('admin.categories.index');
    Route::get('/categories/create', AdminCreateCategoryComponent::class)->name('admin.categories.create');
    Route::get('/categories/edit/{category_slug}', AdminEditCategoryComponent::class)->name('admin.categories.edit');

    Route::get('/products', AdminProductComponent::class)->name('admin.products.index');
    Route::get('/products/create', AdminCreateProductComponent::class)->name('admin.products.create');
    Route::get('/products/edit/{product_slug}', AdminEditProductComponent::class)->name('admin.products.edit');

    Route::get('/slider', AdminHomeSliderComponent::class)->name('admin.home.slider.index');
    Route::get('/slider/create', AdminCreateHomeSliderComponent::class)->name('admin.home.slider.create');
    Route::get('/slider/edit/{slider_id}', AdminEditHomeSliderComponent::class)->name('admin.home.slider.edit');

    Route::get('/home-categories', AdminHomeCategoryComponent::class)->name('admin.home.categories.index');
    Route::get('/sale', AdminSaleComponent::class)->name('admin.sale');

    Route::get('/coupon', AdminCouponComponent::class)->name('admin.coupons.index');
    Route::get('/coupon/add', AdminCreateCouponComponent::class)->name('admin.coupons.create');
    Route::get('/coupon/edit/{coupon_id}', AdminEditCouponComponent::class)->name('admin.coupons.edit');

    Route::get('/orders', AdminOrderComponent::class)->name('admin.orders.index');
    Route::get('/orders/{order_id}', AdminOrderDetailsComponent::class)->name('admin.orders.show');

    Route::get('/contact', AdminContactComponent::class)->name('admin.contacts.index');

    Route::get('/settings', AdminSettingComponent::class)->name('admin.settings.index');
});
